"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration the .env file
dotenv_1.default.config();
// Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define the first route of APP
app.get('/', (req, res) => {
    // Send Hello World 
    res.send('Inicio');
});
// Define the first route of APP
app.get('/hello', (req, res) => {
    // Send Hello World 
    res.send('Hola q tal');
});
// Execute APP and Listen Request to port
app.listen(port, () => {
    console.log('Running in port 8000');
});
//# sourceMappingURL=index.js.map