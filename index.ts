import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

// Configuration the .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port = process.env.PORT || 8000;

// Define the first route of APP
app.get('/', (req: Request, res: Response) => {
    // Send Hello World 
    res.send('Inicio');
});

// Define the first route of APP
app.get('/hello', (req: Request, res: Response) => {
    // Send Hello World 
    res.send('Hola q tal');
});

// Execute APP and Listen Request to port
app.listen(port, () => {
    console.log('Running in port 8000');
}) 