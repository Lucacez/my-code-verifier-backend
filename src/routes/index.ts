import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '@/utils/logger';


let server = express();

let rootRouter = express.Router();

rootRouter.get('/', (req: Request, res: Response) => {
    // Send Hello World 
    res.send('Inicio');
});


server.use('/', rootRouter);
server.use('/hello', helloRouter);

export default server;